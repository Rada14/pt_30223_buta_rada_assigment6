/**
 * AppLogic implements the rules for the given operations
 */
public class AppLogic {
    /**
     * Will insert the given product if we don't have it
     * If we already have this product, it will be checked the price,if the price is equal to the another product then it will be added to the quantity of the product we already have , if not it will be created a new product
     * @param name name of product
     * @param qt quantity of product
     * @param price price of product
     * @return number of inserted rows
     */
    public static int makeInsertOfProduct(String name, int qt, float price)
    {
        Product prod = Find.findProductByName(name);
        if(prod==null)
        {
            int prodID = Product.generateId();
            while(Find.findProductById(prodID)!=null)
            {
                prodID = Product.generateId();
            }
            return Insert.insertProductIntoDatabase(prodID,name, qt, price);
        }
        else
        {
            if (prod.getPrice()==price)
                return Edit.editProductQuantity(prod.getProductID(), prod.getTotalQuantity()+qt);
            else{
                int prodID = Product.generateId();
                return Insert.insertProductIntoDatabase(prodID,name, qt, price);
            }
        }
    }
    /**
     *Inserts a client if we don't have
     * If the client already is in the database  , he won't be added
     * @param name name of client
     * @param city city of client
     * @return number of inserted clients
     */
    public static int makeInsertOfClient(String name, String city)
    {
        Client client = Find.findClientByName(name);
        if(client==null)
        {
            int clientID = Client.generateId();
            while(Find.findClientById(clientID)!=null)
            {
                clientID = Client.generateId();
            }
            return Insert.insertClientIntoDatabase(clientID, name, city);
        }
        else
        {
            return -1;
        }
    }

    /**
     * Making an order for a client , with the quantity wanted of proucts
     * @param name name of client
     * @param product name of product
     * @param quantity wanted quantity for the order
     * @return number of inserted rows
     */
    public static int makeOrder(String name, String product, int quantity)
    {
        int nrOfInsertedRows = 0;
        Client findClient = Find.findClientByName(name);
        Product findProd = Find.findProductByName(product);
        if(findProd==null){
            System.out.println("Nu avem acest produs");
            PDF.createErrorPDF("Nu avem acest produs: " + product);

        }else if(findProd.getTotalQuantity()-quantity<0)
        {
            //error message
            System.out.println("Nu avem destul in stoc pentru a face comanda");
            PDF.createErrorPDF("Nu avem destul in stoc pentru a face comanda" + "(" + product + ", "+ quantity +")");
        }
        else
        {
            Order ord = Find.findOrderByClientId(findClient.getClientId());

            if (ord == null) {
                System.out.println("Inca n-am generat order-ul");

                int orderId = Order.generateOrderId();
                while (Find.findOrderByClientId(orderId)!=null)
                {
                    orderId = Order.generateOrderId();
                }

                Insert.insertOrderIntoDatabase(orderId, findClient.getClientId(), 0);
                ord = Find.findOrderByClientId(findClient.getClientId());

            }

            findProd.setTotalQuantity(findProd.getTotalQuantity()-quantity);
            Edit.editProductQuantity(findProd.getProductID(), findProd.getTotalQuantity());

            if(findProd.getTotalQuantity()==0)
            {
                Delete.deleteProductFromDatabaseById(findProd.getProductID());
            }

            int orderId = ord.getOrderId();
            OrderItem orditm = Find.findIfClientOrderedProduct(orderId, findProd.getProductID());
            if(orditm==null)
                Insert.insertOrderItemIntoDatabase(orderId, findProd.getProductID(), findProd.getNameProd(), quantity);
            else {


                Edit.editOrderedQuantity(orderId, findProd.getProductID(),orditm.getOrderedQuantity()+quantity);
            }

            Edit.editOrderPrice(orderId,ord.getFinalPrice()+quantity*(findProd.getPrice()));
            Bill.createBill(orderId);

        }

        return nrOfInsertedRows;

    }
}
