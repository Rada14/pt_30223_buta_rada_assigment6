
import java.util.Random;

/**
 * Order Class has the information which is required for an order
 */

public class Order {
    /**
     * Random id generated
     */
    private static int randomOrderId = 0;
    /**
     * id
     */
    private int orderId;
    /**
     * the id of client
     */
    private int clientId;
    /**
     * price of the order
     */
    private float finalPrice;



    public static int generateOrderId()
    {
        Random r = new Random();
        randomOrderId = r.nextInt(10000);
        return randomOrderId;
    }

    /**
     * Constructor of the order class
     * @param ord Order ID
     * @param cl the ID of the client that makes the order
     * @param tot total pricde of the order
     */
    public Order(int ord, int cl, float tot) {
        this.orderId = ord;
        this.clientId = cl;
        this.finalPrice = tot;
    }

    /**
     * gets the client ID
     * @param cl the client id
     */

    public void setClientId(int cl) {
        this.clientId = cl;
    }

    public int getClientId() {
        return clientId;
    }

    /**
     * gets the order ID
     * @param oId the order id
     */
    public void setOrderId(int oId) {
        this.orderId = oId;
    }

    public int getOrderId() {
        return orderId;
    }


    /**
     * gets the final price
     * @param fin final price
     * @return  finalPrice  the final price of the product
     */
    public void setFinalPrice(float fin) {
        this.finalPrice = fin;
    }
    public float getFinalPrice() {
        return this.finalPrice;
    }





    public String toString()
    {
        return orderId + " " + clientId + " " + finalPrice;
    }
}
