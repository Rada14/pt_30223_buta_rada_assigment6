/**
 * OrderItem Class provides the information which is required for an ordered item
 */

public class OrderItem {
    /**
     * Order name
     */
    private String productName;
    /**
     * Order id
     */
    private int orderId;
    /**
     * product id
     */
    private int productId;
    /**
     * Ordered quantity
     */
    private int orderedQuantity;

    /**
     * Constructor of OrderItem
     * @param ord order ID
     * @param prodI product ID
     * @param prodN  product NAME
     * @param ordQ  wanted quantity
     */

    public OrderItem(int ord, int prodI, String prodN, int ordQ) {
        this.orderId = ord;
        this.productId = prodI;
        this.productName = prodN;
        this.orderedQuantity= ordQ;
    }
    /**
     * get order id
     * @return   order id
     */
    public void setOrderID(int ord) {
        this.orderId = ord;
    }
    public int getOrderID() {
        return orderId;
    }

    public void setProductId(int pId) {
        this.productId = pId;
    }


    public int getProductId() {
        return this.productId;
    }

    /**
     * get ordered quantity
     * @param orderedQuantity the quan we want
     * @return  ordered quantity
     */
    public void setOrderedQuantity(int orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }

    public int getOrderedQuantity() {
        return this.orderedQuantity;
    }
    /**
     * get product name
     * @param productName name of the product
     * @return  name of the product
     */

    public void setProductName(String productName) {
        this.productName = productName;
    }


    public String getProductName() {
        return productName;
    }


    public String toString()
    {
        return orderId + " " + productId + " " + orderedQuantity;
    }
}
