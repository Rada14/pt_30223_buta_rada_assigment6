import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;




public class PDF {

    /**
     * PDF Class creates  the PDF file we have to create
     */


    /**
     * the name of PDF file that is created
     */
    private String name = "Bill.pdf";


    /**
     * Creates a PDF file in which we will have the information is required for a bill
     * @param result the resultset from the extraction from the database
     */

    public void createBillPDF(ResultSet result)
    {
         Document doc = new Document();
        try {
            PdfWriter.getInstance(doc, new FileOutputStream(name));
            doc.open();

            Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN, 20, BaseColor.RED);
            float tPrice = result.getFloat("o.TotalPrice");
            String clientName = Find.findClientById(result.getInt("o.ClientId")).getName();
            Chunk c1 = new Chunk("Order id : " + result.getInt("o.OrderId"), font);
            Chunk c2 = new Chunk("Client name : " + clientName, font);
            Paragraph p1 = new Paragraph();
            p1.add(c1);
            Paragraph p2 = new Paragraph();
            p2.add(c2);

            ArrayList<String> header = new ArrayList<String>();
            header.add("Product id");
            header.add("Product name");
            header.add("Ordered quantity");
            PdfPTable table = new PdfPTable(header.size());
            createTable(table, header);

            do{

                ArrayList<String> addingRows = new ArrayList<String>();
                addingRows.add(result.getInt("oi.ProductId")+"");
                addingRows.add(result.getString("oi.ProductName"));
                addingRows.add(result.getInt("oi.WQuantity")+"");
                addRows(table, addingRows);
            }while(result.next());

            System.out.println("Total price: " +tPrice +" RON");



            Paragraph p3 = new Paragraph();
            p3.add(table);
            Paragraph p4 = new Paragraph();
            p4.add("Total price: " +tPrice +" RON");

            Paragraph pSpace = new Paragraph();

            pSpace.add("\n");

            Chunk tChunk = new Chunk("Bill:", font);

            doc.add(tChunk);
            doc.add(pSpace);
            doc.add(p1);
            doc.add(p2);
            doc.add(pSpace);
            doc.add(p3);
            doc.add(pSpace);
            doc.add(p4);
            doc.close();
        } catch (DocumentException exc) {
            exc.printStackTrace();
        } catch (FileNotFoundException exc) {
            exc.printStackTrace();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
    }

    /**
     * Creates a PDF file with all the clients we have in the database
     * @param result the resultset from the extraction from the database
     */

    public void createReportClientPDF(ResultSet result)
    {
        Document doc = new Document();
        Document errDoc = new Document();
        try {
            if(result==null)
            {
                PdfWriter.getInstance(errDoc, new FileOutputStream("Eroare Raport Clienti" + + Client.generateId()+".pdf"));
                errDoc.open();
                Paragraph er = new Paragraph();
                er.add("No clients");
                errDoc.add(er);
                errDoc.close();
            }else {
                PdfWriter.getInstance(doc, new FileOutputStream(name));
                doc.open();

                Paragraph p1 = new Paragraph();
                p1.add("Report Clients");

                ArrayList<String> header = new ArrayList<String>();

                header.add("ClientId");
                header.add("Name");
                header.add("City");

                PdfPTable table = new PdfPTable(header.size());
                createTable(table, header);

                do {
                    ArrayList<String> rows = new ArrayList<String>();
                    rows.add(result.getInt("ClientID") + "");
                    rows.add(result.getString("Name"));
                    rows.add(result.getString("City") + "");

                    addRows(table, rows);
                } while (result.next());

                Paragraph pSpace = new Paragraph();
                pSpace.add("\n");

                Paragraph p3 = new Paragraph();
                p3.add(table);


                doc.add(p1);
                doc.add(pSpace);
                doc.add(p3);
                doc.close();
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a PDF file which contains all the products in the database
     * @param result  the resultset from the extraction from the database
     */
    public void createReportProductPDF(ResultSet result)
    {
        Document doc = new Document();
        Document errDoc = new Document();
        try {

            if(result==null)
            {        Paragraph er = new Paragraph();
                     er.add("No products");
                PdfWriter.getInstance(errDoc, new FileOutputStream("Error product"+ Client.generateId()+".pdf"));
                errDoc.open();


                errDoc.add(er);
                errDoc.close();
            }else {
                PdfWriter.getInstance(doc, new FileOutputStream(name));
                doc.open();
                Paragraph p1 = new Paragraph();
                p1.add("Report Products");
                ArrayList<String> header = new ArrayList<String>();
                header.add("ProductId");
                header.add("Name of Product");
                header.add("Total Quantity");
                header.add("Price");
                PdfPTable table = new PdfPTable(header.size());
                createTable(table, header);
                 while (result.next())
                 {
                    ArrayList<String> rows= new ArrayList<String>();
                    rows.add(result.getInt("ProductID") + "");
                    rows.add(result.getString("NameProd"));
                    rows.add(result.getInt("TQuantity") + "");
                    rows.add(result.getFloat("Price") + "");
                    addRows(table, rows);
                }


                Paragraph pSpace = new Paragraph();
                pSpace.add("\n");

                Paragraph p3 = new Paragraph();
                p3.add(table);


                doc.add(p1);
                doc.add(pSpace);
                doc.add(p3);
                doc.close();
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a PDF file which contains all the  orders we have
     * @param result  the resultset from the extraction from the database
     */
    public void createReportOrderPDF(ResultSet result)
    {
        Document doc = new Document();
        Document errDoc = new Document();
        try {


            if(result==null)
            {
                PdfWriter.getInstance(errDoc, new FileOutputStream("Error orders report"+Client.generateId()+".pdf"));
                errDoc.open();
                Paragraph er= new Paragraph();
                er.add("No orders");
                errDoc.add(er);
                errDoc.close();
            }else {
                PdfWriter.getInstance(doc, new FileOutputStream(name));
                doc.open();
                Paragraph p1 = new Paragraph();
                p1.add("Report Orders");
                ArrayList<String> header = new ArrayList<String>();
                header.add("OrderID");
                header.add("ClientID");
                header.add("Total Price");
                PdfPTable table = new PdfPTable(header.size());
                createTable(table, header);

                while(result.next())
                {
                    ArrayList<String> rows = new ArrayList<String>();
                    rows.add(result.getInt("OrderId") + "");
                    rows.add(result.getInt("ClientId") + "");
                    rows.add(result.getInt("TotalPrice") + "");
                    addRows(table, rows);
                }

                Paragraph pSpace = new Paragraph();
                pSpace.add("\n");

                Paragraph p3 = new Paragraph();
                p3.add(table);


                doc.add(p1);
                doc.add(pSpace);
                doc.add(p3);
                doc.close();
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a PDF with a message
     * @param msg the message that we want to show in the pdf we create
     */

    public static void createErrorPDF(String msg)
    {
        Document errDoc = new Document();
        try {
            PdfWriter.getInstance(errDoc, new FileOutputStream("Error" + Client.generateId() + ".pdf"));
            errDoc.open();
            Paragraph er = new Paragraph();
            er.add(msg);
            errDoc.add(er);
            errDoc.close();
        }catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }catch(DocumentException e)
        {
            e.printStackTrace();
        }
    }


    /**
     * Creates the headers for the table
     * @param table the table which  will have given headers
     * @param header the titles of headers
     */

    private void createTable(PdfPTable table, ArrayList<String> header)
    {
        for(String s:header) {
            PdfPCell h1 = new PdfPCell();
            h1.setBorderWidth(2);
            h1.setBackgroundColor(BaseColor.LIGHT_GRAY);
            h1.setPhrase(new Phrase(s));
            table.addCell(h1);
        }
    }

    private void addRows(PdfPTable table, ArrayList<String> rows) {

        for(String s:rows)
        {
            table.addCell(s);
        }
    }


    public void setName(String nume)
    {
         name= nume;
    }
}
