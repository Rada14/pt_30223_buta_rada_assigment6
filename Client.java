import java.util.Random;

public class Client {
    /**
     *
     * information required for a client : id, name,city
     */
    private int clientId;
    private String name;
    private String city;

    /**
     * here we generate a random number between 0 and 10000
     * @return the generated number
     */
    public static int generateId()
    {
        Random random = new Random();
        int genId = random.nextInt(10000);
        return genId;
    }

    /**
     * We have this constructor for initializating the Client parameters
     * @param cl Client ID
     * @param n  Name
     * @param c  City
     */
    public Client(int cl, String n, String c) {
        this.clientId = cl;
        this.name = n;
        this.city = c;
    }

    public void setName(String n) {
        this.name = n;
    }

    public String getName() {
        return name;
    }


    /**
     * get ID of client
     * @param cl
     * @return clientId
     */
    public void setClientId(int cl) {

        this.clientId = cl;
    }
    public int getClientId() {

        return this.clientId;
    }




    public void setCity(String c)
    {
        this.city = c;
    }



    public String getCity() {
        return this.city;
    }

    /**
     *
     * @return all the information  about the client
     */


    public String toString()

    {
        return clientId + " " + name + " " + city;
    }



}
