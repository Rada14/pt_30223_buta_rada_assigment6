import java.util.Random;


public class Product {

    private int productID;
    private String nameProd;
    private int totalQuantity;
    private float price;

    public static int generateId()
    {
        Random random = new Random();
        int genId = random.nextInt(10000);
        return genId;
    }

    public Product(int p, String n, int t, float pr) {
        this.productID = p;
        this.nameProd = n;
        this.totalQuantity = t;
        this.price = pr;
    }
    public void setNameProd(String na) {
        this.nameProd = na;
    }
    public String getNameProd() {
        return nameProd;
    }

    public void setProductID(int prod) {
        this.productID = prod;
    }


    public int getProductID() {
        return productID;
    }


    public void setTotalQuantity(int t) {
        this.totalQuantity = t;
    }
    public int getTotalQuantity() {
        return this.totalQuantity;
    }

    public void setPrice(float pr) {
        this.price = price;
    }


    public float getPrice() {
        return this.price;
    }


    public String toString()
    {
        return productID + " " + nameProd + " " + totalQuantity + " " + price;
    }



}
