import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * ReadFile Class reads a file
 */
public class ReadFile {
    /**
     * Reads from a file and executes the instructions of that file
     * @param path the path of the file
     * The instructions we can execute are the ones we implememented in this code ( insert , delete , etc . )

     */
  
    public static void readFile(String path)
    {
        File readFile = new File(path);
        String readLine;
        try
        {
            Scanner scan = new Scanner(readFile );
            while (scan.hasNextLine()) {
                readLine = scan.nextLine();
                if(readLine.contains("Insert")){ 
                    if(readLine.contains("client")) 
                    {
                        String allData = readLine.substring(15);
                        String[] inputData = allData.split(", ");
                        AppLogic.makeInsertOfClient(inputData[0], inputData[1]);
                    }else if(readLine.contains("product"))
                    {
                        String allData = readLine.substring(16);
                        String[] inputData = allData.split(", ");
                        int tq = Integer.parseInt(inputData[1]);
                        float price = Float.parseFloat(inputData[2]);
                        AppLogic.makeInsertOfProduct(inputData[0], tq, price);
                    }

                } else if(readLine.contains("Delete")){ 
                    if(readLine.contains("client"))
                    {
                        String allData = readLine.substring(15);
                        Delete.deleteClientFromDatabase(allData);
                    }else if(readLine.contains("product"))
                    {
                        String allData = readLine.substring(16);
                        Delete.deleteProductFromDatabase(allData);
                    }
                } else if(readLine.contains("Order")){ 
                    String allData = readLine.substring(7);
                    String[] inputData = allData.split(", ");
                    int qt = Integer.parseInt(inputData[2]);
                    AppLogic.makeOrder(inputData[0], inputData[1], qt);
                } else if(readLine.contains("Report")) 
                {
                    if(readLine.contains("client"))
                    {
                        Find.reportClientDatabase();
                    }else if(readLine.contains("product"))
                    {
                        Find.reportProductDatabase();
                    }else if(readLine.contains("order"))
                    {
                        Find.reportOrderDatabase();
                    }
                }
            }
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Doesn't exist");
          
        }
    }
}
