import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class Delete {
    /**
     * the command for deleting a client by the name
     */
    private static final String deleteClient = "delete from database.Client where Name = ?;";
    /**
     * the command for deleting the product by the name we choose
     */
    private static final String deleteProductByName = "delete from database.Product where NameProd = ?;";
    /**
     * the command for deleting a product by the product id
     */

    private static final String deleteProductById = "delete from database.Product where ProductID = ?;";
    /**
     * Deletes a client by the name we choose
     * @param name name of client
     * @return number of deleted rows
     */
    public static int deleteClientFromDatabase(String name)
    {
        int nrRows = 0;
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement statement;
        statement= null;

        try{
            statement = connect.prepareStatement(deleteClient);
            statement.setString(1, name);
            nrRows = statement.executeUpdate();

        }catch (SQLException exc)
        {
            System.out.println("Error deleting client: " + exc);
        }

        return nrRows;
    }

    /**
     * Deletes a product by the name we want
     * @param name name of product
     * @return number of deleted rows
     */

    public static int deleteProductFromDatabase(String name)
    {
        int nrRows = 0;
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement statement;
        statement= null;

        try{
            statement = connect.prepareStatement(deleteProductByName);
            statement.setString(1, name);
            nrRows = statement.executeUpdate();

        }catch (SQLException exc)
        {
            System.out.println("Error deleting product: " + exc);
        }

        return nrRows;
    }


    /**
     * Deletes a product by the id we choose
     * @param id id of hte product
     * @return number of deleted rows
     */

    public static int deleteProductFromDatabaseById(int id)
    {
        int nrRows = 0;
        Connection connect= ConnectionFactory.getConnection();
        PreparedStatement statement;
        statement= null;

        try{
            statement = connect.prepareStatement(deleteProductById);
            statement.setInt(1, id);
            nrRows = statement.executeUpdate();

        }catch (SQLException exc)
        {
            System.out.println("Error deleting product by id: " + exc);
        }

        return nrRows;
    }
}
