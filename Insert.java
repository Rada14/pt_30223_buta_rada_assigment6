import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * Insert Class has methods that execute SQL insert statements
 */

public class Insert {
    /**
     * command for inserting client in sql
     */
    private static final String insertClient = "insert into database.Client values(?, ?, ?);";
    /**
     * command for inserting product in sql
     */
    private static final String insertProduct = "insert into database.Product values(?, ?, ?, ?);";
    /**
     * command for inserting order in sql
     */
    private static  final String insertOrder = "insert into database.Order values(?, ?, ?);";
    /**
     * command for inserting orderItem in sql
     */
    private static final String insertOrderItem = "insert into database.OrderItem values(?, ?, ?, ?);";


    /**
     * Executes the inseration of client in database
     * @param clId client id that is inserted
     * @param n name that is inserted for the client
     * @param c city that is inserted for the client
     * @return number of inserted rows
     */
    public static int insertClientIntoDatabase(int clId, String n, String c)
    {
        int nrRows=0;
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement statement;
        statement= null;

        try{
            nrRows=0;
            statement = connect.prepareStatement(insertClient);
            statement.setLong(1, clId);
            statement.setString(2, n);
            statement.setString(3, c);
            nrRows = statement.executeUpdate();

        }catch (SQLException exc)
        {
            System.out.println("Error inserting client :  " + exc);
        }

        return nrRows;
    }
    /**
     * Executes the inseration of  product in database
     * @param prodId  id that is inserted for the product
     * @param n name that is inserted
     * @param quant quantity which is inserted
     * @param pr price for the product
     * @return number of inserted rows
     */

    public static int insertProductIntoDatabase(int prodId, String n, int quant, float pr)
    {
        int nrRows =0 ;
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement  statement ;
        statement = null;

        try{
            statement = connect.prepareStatement(insertProduct);
            statement.setLong(1, prodId);
            statement.setString(2, n);
            statement.setLong(3, quant);
            statement.setFloat(4, pr);

            nrRows = statement.executeUpdate();

        }catch (SQLException exc)
        {
            System.out.println("Error inserting product: " + exc);
        }

        return nrRows;
    }

    /**
     * Executes the inseration of order in database
     * @param oId order  id that is inserted
     * @param  cId client id  that is inserted for the order
     * @param pr price that is inserted
     * @return number of inserted rows
     */
    public static int insertOrderIntoDatabase(int oId, int cId, float pr)
    {
        int nrRows = 0;
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement  statement;
                statement = null;

        try{
            statement = connect.prepareStatement(insertOrder);
            statement.setLong(1, oId);
            statement.setLong(2, cId);
            statement.setFloat(3, pr);

            nrRows = statement.executeUpdate();

        }catch (SQLException exc)
        {
            System.out.println("Error inserting order: " + exc);
        }

        return nrRows;
    }

    /**
     * Executes the inseration of orderItem in database
     * @param oId order  id that is inserted
     * @param pId product id  that is inserted
     * @param pName name of the product  that is inserted
     * @param ordQuantity quantity which is ordered
     * @return number of inserted rows
     */

    public static int insertOrderItemIntoDatabase(int oId, int pId, String pName, int ordQuantity)
    {
        int nrRows = 0;
        Connection connect = ConnectionFactory.getConnection();
        PreparedStatement statement;
                statement = null;

        try{
            statement = connect.prepareStatement(insertOrderItem);
            statement.setLong(1, oId);
            statement.setLong(2, pId);
            statement.setString(3, pName);
            statement.setLong(4, ordQuantity);

            nrRows = statement.executeUpdate();

        }catch (SQLException ex)
        {
            System.out.println("Error : " + ex);
        }

        return nrRows;
    }

}
